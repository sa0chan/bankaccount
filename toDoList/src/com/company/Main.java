package com.company;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner inputUser = new Scanner(System.in);
        Boolean isReadyToQuit= false ;

        //Création d'une todolist

        ArrayList<Task> toDoListName = new ArrayList<Task>();
        System.out.println("Rentrez vos tâches: ");
        toDoListName.add(new Task(inputUser.nextLine()));

//tant que is ReadytoQuit n'est pas à true alors on continue la boucle .
        while(!isReadyToQuit){

            System.out.println("Avez vous une autre tâche à ajouter ? y/n");

            if(inputUser.nextLine().equals("n")){
                System.out.println(" Fin de la création de votre liste ");
                isReadyToQuit= true ;
            }else{
                System.out.println("Rentrez une autre tâche : ");
                toDoListName.add(new Task(inputUser.nextLine()));
            }

        }


        System.out.println("Voici vos tâches à faire");


        for (Task taskToDo : toDoListName){
            taskToDo.saveTaskOnFile(taskToDo);
            System.out.println(taskToDo.getTaskDescription());
        }

    }

}
