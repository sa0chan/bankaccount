package com.company;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Task {

    public String taskDescription;
    //Date taskEndDate;

    public Task(String description){
        taskDescription = description;

        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
       // Date taskEndDate = sdf.parse(taskEndDate);

    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void saveTaskOnFile(Task taskToWrite) {
        try (FileWriter fw = new FileWriter("toDoList.txt", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(taskToWrite.getTaskDescription());
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

}

