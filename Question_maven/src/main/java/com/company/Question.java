package com.company;

import org.apache.commons.text.similarity.LevenshteinDistance;

class Question {
    protected String answerText;
    protected String questionText;
    protected Boolean compareAnswer ;
    LevenshteinDistance levenshteinDistance = new LevenshteinDistance();

    Question( String question, String answer) {
        questionText = question;
        answerText = answer;

    }
//recupérer la question
    public String getQuestionText() {
        return questionText;
    }


//récupérer la réponse
    public Boolean getAnswerText(String input) {
        compareAnswer = levenshteinDistance.apply(answerText.toLowerCase(), input.toLowerCase()) <= 2;
        return compareAnswer;
    }

}












