package com.company;
import java.util.ArrayList;
import java.util.Scanner;

class Manager { //Class Manager qui va gérer les interactions client

    // création de la liste vide de l'objet : Question
    ArrayList<Question> questionsList = new ArrayList<Question>();
    //Instanciation du scanner pour les inputs clients.
    Scanner sc = new Scanner(System.in);


    User currentUser = new User();

    // premiere méthode du Manager qui est éxécutée au démarage dans Main
    public void start() {

        //parametrage des questions ouvertes
        String question1 ="Quel est la couleur du cheval blanc d'Henri IV ?";
        String reponse1 = "Blanc" ;

        String questionQcm1 = "Quel est la capitale de la France ?";
        String reponseQcm1 = "Paris";

        //parametrage questions vrai/faux
        String questionTrueFalse1 = "Capgémini est-il le nom du criquet dans Pinocchio ?";
        Boolean reponseTrueFalse1 = false ;

        String questionTrueFalse2 = " Rennes est en Bretagne ?";
        Boolean reponseTrueFalse2 = true ;


        //parametrage questions QCM
        ArrayList<String>choiceReponseQcm1 = new ArrayList<String>();
        choiceReponseQcm1.add("Paris");
        choiceReponseQcm1.add("Londres");
        choiceReponseQcm1.add("Berlin");
        choiceReponseQcm1.add("Quebec");

        int indexReponseQcm1 = 0;

        //paramétrage des listes de questions
        questionsList.add(new Question(question1,reponse1));
        questionsList.add(new Qcm(questionQcm1, choiceReponseQcm1, indexReponseQcm1 ));
        questionsList.add(new QuestionVraiFaux(questionTrueFalse1, reponseTrueFalse1));
        questionsList.add(new QuestionVraiFaux(questionTrueFalse2, reponseTrueFalse2));


        //Lancement

        //choix des joueurs
        /*System.out.println("Bienvenue ! Combien de joueurs veulent jouer ?");
        currentUser.setUserName(sc.nextLine());*/

        System.out.println("Quel est votre nom ?");
        currentUser.setUserName(sc.nextLine());

       //lancement du questionnaires
        this.startGame();
    }

    private void startGame() {

        int score = 0;

        for (Question currentQuestion : questionsList) {
            System.out.println(currentQuestion.getQuestionText());
            // si c'est true alors c'est la bonne réponse donc augmentation du score
            if(currentQuestion.getAnswerText(sc.nextLine())){
                score++;
                currentUser.setUserScore(score);
            }
        }

        if(score == questionsList.size()){
            System.out.println("Bravo, " + currentUser.getUserName() +  "! Votre score est " + currentUser.getUserScore() + ".");
        }
        else{
            System.out.println("Ca n'est pas fameux, " + currentUser.getUserName() +  ", votre score est " + currentUser.getUserScore() + "...");
        }
    }
}
