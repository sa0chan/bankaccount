package com.company;

import java.util.ArrayList;

public class QuestionVraiFaux extends Question {

    private ArrayList<String> listAnswerFalse;
    private ArrayList<String> listAnswerTrue;
    private Boolean realAnswer ;

    QuestionVraiFaux(String question , Boolean answer){
        super(question, Boolean.toString(answer));
        realAnswer = answer;
    }

    @Override

    public String getQuestionText(){

        return   "Vrai ou Faux ?" + System.lineSeparator() + questionText;

    }

    @Override

    public Boolean getAnswerText(String input){

        //création liste avec differents terme possible
        listAnswerFalse = new ArrayList<String>();
        listAnswerFalse.add("non");
        listAnswerFalse.add("faux");
        listAnswerFalse.add("false");

        listAnswerTrue = new ArrayList<String>();
        listAnswerTrue.add("vrai");
        listAnswerTrue.add("true");
        listAnswerTrue.add("oui");

        // permet de retourner le bon tableau en fonction de la vrai réponse et de voir si l'input correspond à ce tableau
        if(realAnswer){
            compareAnswer = (listAnswerTrue.contains(input.toLowerCase()));
        }else{
            compareAnswer = (listAnswerFalse.contains(input.toLowerCase()));
        }
        return compareAnswer;

    }


}
