package com.company;

import java.util.ArrayList;

public class Qcm extends Question {

    private ArrayList<String> choicesList;
    private int indexAnswer;

    Qcm(String question, ArrayList<String> multiAnswer, Integer indexOfAnswer) {

        super(question, Integer.toString(indexOfAnswer));
        choicesList = multiAnswer;
        indexAnswer = indexOfAnswer;

    }


    public String getQuestionText() {
        String s = questionText;

        for( int i = 0 ; i < choicesList.size() ; i++){
             s += System.lineSeparator() + (i+1) + " ) "  +
                     choicesList.get(i);
        }
        return s ;
    }



    @Override

    public Boolean getAnswerText(String input){

        String trueChoice = choicesList.get(indexAnswer);

        if(input.equals(Integer.toString(indexAnswer+1)) || input.toLowerCase().equals(trueChoice.toLowerCase())){
            return true;
        }else{
            return false ;
        }

    }

}