package com.company;

public class MonetaryAmount {
    private double amount;
    public  String currency ;

    public MonetaryAmount(double amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    //ajouter de l'argent
    public void addAmount(double a){
        amount = amount + a ;
    }

    //retirer de l'argent
    public void substractAmount(double a){

            amount = amount - a  ;

    }

    public double getAmount() {
        return amount;
    }

    //convertir  en chaine de caractères 
    public String toString(){

        return Double.toString(getAmount());

    }
}
