package com.company;

public class Main {

    public static void main(String[] args) {
	    //création d'un compte en banque
        Account account = new Account("Obélix","sesterces");
        //affichage du montant disponible
        account.getCurrentBalance();
        //ajout de 500
        account.deposit(500.5);
        // affichage du montant
        account.getCurrentBalance();
        // retirer de l'argent
        account.withDraw(125.25);
        // affichage du montant
        account.getCurrentBalance();
        // retirer de l'argent
        account.withDraw(1337.25);
        // affichage du montant
        account.getCurrentBalance();

    }
}
